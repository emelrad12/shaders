#pragma once

class HeatTransfer
{
public:
	std::vector<std::vector<float>>& Next();
	HeatTransfer(int gridSize);
private:
	static inline float CalculateHeatFlowAtLocation(std::vector<std::vector<float>>& grid, std::vector<float>& columnSave, int x, int y, float rate);
	static std::vector<std::vector<float>> InitArray(int sizeY, int sizeX);
	std::vector<std::vector<float>> gridStep;
	std::vector<std::vector<float>> columnSave;
	int itemsPerThread;
	int gridDimension;
};