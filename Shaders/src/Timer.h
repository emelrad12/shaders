#pragma once
#include <chrono>
#include <string>

class Timer
{
public:
	void start();
	void stop();
	Timer(std::string name);
private:
	std::string name;
	std::chrono::steady_clock::time_point begin;
};

