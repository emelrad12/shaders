#pragma once
#include "raytracing/RaytracingGlobals.h"
class CharacterController
{
public:
	void Update();
	bool changed = false;
	Vector3f position = Vector3f(0,0,0);
	Quaternionf rotation = Quaternionf().Identity();
	Vector3f rotationEuler = Vector3f(0, 0, 0);
};