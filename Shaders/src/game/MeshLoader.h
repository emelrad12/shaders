#pragma once
class World;

class MeshLoader
{
public:
	void Load(World& world, std::string path);
};
