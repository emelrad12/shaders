#include "MeshLoader.h"
#include "FileLoader.h"
#include "raytracing/Physics/World.h"

void MeshLoader::Load(World& world, std::string path)
{
	objl::Loader Loader;
	bool loadout = Loader.LoadFile(path);
	if (loadout)
	{
		for (auto mesh : Loader.LoadedMeshes)
		{
			auto id = world.meshes.AddMesh(Vector3f(0, 0, 0), world.GetMaterial("red").index);
			
			for (auto vertex : mesh.Vertices)
			{
				world.meshes.AddVertex(id, vertex.Position.X, vertex.Position.Y, vertex.Position.Z);
			}
			
			for (int i = 0; i < mesh.Indices.size(); i += 3)
			{
				world.meshes.AddTriangle(id, mesh.Indices[i], mesh.Indices[i + 1], mesh.Indices[i + 2]);
			}
		}
	}
}
