#include "CharacterController.h"
#include "windows.h"

void CharacterController::Update()
{
	auto mult = 1.0f;
	auto rotMult = 0.001f;
	changed = false;
	if (GetKeyState(VK_SHIFT) & 0x8000)
	{
		mult *= 5;
		rotMult *= 5;
	}
	if (GetKeyState(VK_CONTROL) & 0x8000)
	{
		mult /= 5;
		rotMult /= 5;
	}
	if (GetKeyState('W') & 0x8000)
	{
		position += rotation * (Vector3f(0, 0, 1) * mult);
		changed = true;
	}
	if (GetKeyState('S') & 0x8000)
	{
		position += rotation * (Vector3f(0, 0, -1) * mult);
		changed = true;
	}
	if (GetKeyState('D') & 0x8000)
	{
		position += rotation * (Vector3f(1, 0, 0) * mult);
		changed = true;
	}
	if (GetKeyState('A') & 0x8000)
	{
		position += rotation * (Vector3f(-1, 0, 0) * mult);
		changed = true;
	}
	if (GetKeyState('E') & 0x8000)
	{
		position += rotation * (Vector3f(0, 1, 0) * mult);
		changed = true;
	}
	if (GetKeyState('Q') & 0x8000)
	{
		position += rotation * (Vector3f(0, -1, 0) * mult);
		changed = true;
	}

	if (GetKeyState(VK_UP) & 0x8000)
	{
		rotationEuler.x() += +1 * rotMult;
		changed = true;
	}
	if (GetKeyState(VK_DOWN) & 0x8000)
	{
		rotationEuler.x() += -1 * rotMult;
		changed = true;
	}
	if (GetKeyState(VK_RIGHT) & 0x8000)
	{
		rotationEuler.y() += +1 * rotMult;
		changed = true;
	}
	if (GetKeyState(VK_LEFT) & 0x8000)
	{
		rotationEuler.y() += -1 * rotMult;
		changed = true;
	}
	rotation = Eigen::AngleAxis(rotationEuler.z(), Vector3f::UnitZ()) * Eigen::AngleAxis(rotationEuler.y(), Vector3f::UnitY()) * Eigen::AngleAxis(rotationEuler.x(), Vector3f::UnitX());
}
