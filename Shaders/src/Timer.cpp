#include "stdafx.h"
#include "Timer.h"
#include <iostream>
#include <chrono>
#include <iomanip>

void Timer::start()
{
	begin = std::chrono::high_resolution_clock::now();
}

void Timer::stop()
{
	auto end = std::chrono::high_resolution_clock::now();
	auto dur = end - begin;
	auto ms = std::chrono::duration_cast<std::chrono::microseconds>(dur).count();
	std::cout << name << ": " << ms << std::endl;
}

Timer::Timer(std::string name)
{
	Timer::name = name;
}
