#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cuda_gl_interop.h>
#include "../raytracing/Cuda/CudaGlobals.h"

float* GetDevicePointerFromVertexBuffer(unsigned int bufferId)
{
	cudaGraphicsResource_t resource = 0;
	cudaGraphicsGLRegisterBuffer(&resource, bufferId, cudaGraphicsMapFlagsNone);
	cudaGraphicsMapResources(1, &resource);
	void* devicePointer;
	size_t size;
	checkCudaError(cudaGraphicsResourceGetMappedPointer(&devicePointer, &size, resource));
	return static_cast<float*>(devicePointer);
}
