#include "stdafx.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include "Renderer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include "../ompMain.h"
#include <vector>
#include "OmpRenderer.h"
#include "../Timer.h"

void DrawHeat()
{
	Renderer renderer;
	glfwInit();
	GLFWwindow* window = glfwCreateWindow(1000, 1000, "", nullptr, nullptr);

	glfwMakeContextCurrent(window);
	glewInit();

	std::cout << glGetString(GL_VERSION);
	{
		const int gridSize = 1000;
		HeatTransfer heatTransfer(gridSize);
		unsigned int indiciesCount = gridSize * gridSize;
		auto positionsCount = gridSize * gridSize;
		auto positions = new float[gridSize * gridSize * 3];
		Shader shader("src/shaders/heat/pixel");

		auto indices = new unsigned int[gridSize * gridSize];
		for (auto i = 0; i < gridSize; i++)
		{
			for (auto j = 0; j < gridSize; j++)
			{
				indices[i * gridSize + j] = i * gridSize + j;
			}
		}
		
		IndexBuffer ib(indices, indiciesCount);
		VertexArray va;
		VertexBuffer vb(positions, positionsCount * 3 * sizeof(float));
		VertexBufferLayout layout;
		layout.Push<float>(2);
		layout.Push<float>(1);
		va.AddBuffer(vb, layout);
		Timer timer("OpenGl");
		while (!glfwWindowShouldClose(window))
		{
			timer.start();
			auto heatResult = heatTransfer.Next();
			for (auto i = 0; i < gridSize; i++)
			{
				for (auto j = 0; j < gridSize; j++)
				{
					positions[(i * gridSize + j) * 3] = static_cast<float>(i) / gridSize * 2 - 1;
					positions[(i * gridSize + j) * 3 + 1] = static_cast<float>(j) / gridSize * 2 - 1;
					positions[(i * gridSize + j) * 3 + 2] = heatResult[i][j] / 1000;
				}
			}
			vb.UpdateData();

			renderer.Clear();
			shader.Bind();

			renderer.DrawPixels(va, ib, shader);

			glfwSwapBuffers(window);
			glfwPollEvents();
			timer.stop();
		}
	}
	glfwTerminate();
}
