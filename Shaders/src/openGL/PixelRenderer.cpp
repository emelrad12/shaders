#pragma once
#include "stdafx.h"
#include "PixelRenderer.h"
#include <driver_types.h>
#include "Renderer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include "../Timer.h"
#include "../raytracing/Cuda/CudaGlobals.h"


void Render(PixelRendable& objectToRender)
{
	Renderer renderer;
	glfwInit();
	const int xSize = objectToRender.xSize;
	const int ySize = objectToRender.ySize;
	GLFWwindow* window = glfwCreateWindow(xSize, ySize, "", nullptr, nullptr);

	glfwMakeContextCurrent(window);
	glewInit();
	{
		const unsigned int indiciesCount = xSize * ySize;
		const unsigned int positionsCount = xSize * ySize;
		Shader shader("src/shaders/pixel/pixel");
		shader.Bind();

		auto indices = new unsigned int[xSize * ySize];
		for (auto i = 0; i < xSize; i++)
		{
			for (auto j = 0; j < ySize; j++)
			{
				indices[i * ySize + j] = i * ySize + j;
			}
		}

		IndexBuffer ib(indices, indiciesCount);
		VertexArray va;
		VertexBufferLayout layout;
		layout.Push<float>(2);
		layout.Push<float>(3);
		auto* const vertices = new float[xSize * ySize * layout.size];
		VertexBuffer vb(vertices, positionsCount * layout.size * sizeof(float));
		va.AddBuffer(vb, layout);
		Timer timer("TotalRenderTime");
		timer.start();
		objectToRender.RegisterOpenGlVertexBuffer(vb.m_RendererID);

		while (!glfwWindowShouldClose(window))
		{
			while(!objectToRender.finished)
			{
				objectToRender.Update();
				objectToRender.IncrementBlock();
			}
			
			renderer.Clear();
			shader.Bind();
			renderer.DrawPixels(va, ib, shader);
			glfwSwapBuffers(window);
			glfwPollEvents();
			objectToRender.finished = false;
			timer.stop();
			timer.start();
			// exit(0);
		}
	}
	glfwTerminate();
}
