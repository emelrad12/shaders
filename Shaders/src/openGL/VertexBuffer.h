#pragma once

class VertexBuffer
{
private:
	const void* lastDataPointer;
	unsigned int lastDataSize;
public:
	unsigned int m_RendererID;
	VertexBuffer(const void* data, unsigned int size);
	~VertexBuffer();

	void Bind() const; 
	void Unbind() const;

	void UpdateData();
};
