#include "stdafx.h"
#include "Shader.h"
#include "Renderer.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

Shader::Shader(const std::string& filepath): m_FilePath(filepath), m_RendereID(0)
{
	ShaderProgramSource source = ParseShader(filepath);
	m_RendereID = CreateShader(source.VertexSource, source.FragmentSource);
}

Shader::~Shader()
{
	// GLCall(glDeleteShader(m_RendereID))
	//todo fix
}

ShaderProgramSource Shader::ParseShader(const std::string& filepath)
{
	std::stringstream ss[2];
	{
		std::string line;
		std::ifstream stream(filepath + ".frag");
		while (getline(stream, line))
		{
			ss[1] << line << '\n';
		}
	}
	{
		std::ifstream stream(filepath + ".vert");
		std::string line;
		while (getline(stream, line))
		{
			ss[0] << line << '\n';
		}
	}
	return {ss[0].str(), ss[1].str()};
}

unsigned int Shader::CompileShader(unsigned int type, const std::string& source)
{
	GLCall(unsigned int id = glCreateShader(type));
	GLCall(const char* src = source.c_str());
	GLCall(glShaderSource(id, 1, &src, nullptr));
	GLCall(glCompileShader(id));

	int result;
	GLCall(glGetShaderiv(id, GL_COMPILE_STATUS, &result));
	if (result == GL_FALSE)
	{
		int lenght;
		GLCall(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &lenght));
		char* message = (char*)alloca(lenght * sizeof(char));
		GLCall(glGetShaderInfoLog(id, lenght, &lenght, message));
		std::cout << message << std::endl;
		std::cout << "Failed to compile " << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << std::endl;
		GLCall(glDeleteShader(id));
		return 0;
	}
	return id;
}


unsigned int Shader::CreateShader(const std::string& vertexShader, const std::string& fragmentShader)
{
	GLCall(unsigned int program = glCreateProgram());
	GLCall(unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader));
	GLCall(unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader));

	GLCall(glAttachShader(program, vs));
	GLCall(glAttachShader(program, fs));
	GLCall(glLinkProgram(program));
	GLCall(glValidateProgram(program));
	GLCall(glDeleteShader(vs));
	GLCall(glDeleteShader(fs));

	return program;
}

void Shader::Bind() const
{
	GLCall(glUseProgram(m_RendereID));
}

void Shader::Unbind() const
{
	GLCall(glUseProgram(0));
}

void Shader::SetUniform4f(const std::string& name, float v0, float v1, float v2, float v3)
{
	auto location = GetUnitformLocation(name);
	GLCall(glUniform4f(location, v0, v1, v2, v3));
}

void Shader::SetUniform1i(const std::string& name, int v0)
{
	auto location = GetUnitformLocation(name);
	GLCall(glUniform1i(location, v0));
}

void Shader::SetUniform1f(const std::string& name, float v0)
{
	auto location = GetUnitformLocation(name);
	GLCall(glUniform1f(location, v0));
}

void Shader::SetUniformMat4f(const char* name, const glm::mat4 matrix)
{
	GLCall(glUniformMatrix4fv(GetUnitformLocation(name), 1, GL_FALSE, &matrix[0][0]));
}

int Shader::GetUnitformLocation(const std::string& name)
{
	if (m_UniformLocationCache.find(name) != m_UniformLocationCache.end())
	{
		return m_UniformLocationCache[name];
	}
	GLCall(int location = glGetUniformLocation(m_RendereID, name.c_str()));
	if (location == -1)
	{
		std::cout << "Warning: uniform '" << name << "'does not exist!" << std::endl;
	}
	m_UniformLocationCache[name] = location;
	return location;
}
