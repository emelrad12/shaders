#pragma once
#include "../Timer.h"
#include "raytracing/RaytracingGlobals.h"
extern  float* GetDevicePointerFromVertexBuffer(unsigned int bufferId);
class PixelRendable
{
public:
	PixelRendable(){
		timer.start();
	}
	virtual void Update() = 0;
	void IncrementBlock()
	{
		blockId++;
		currentBlockY += blockSize;
		if (currentBlockY >= ySize)
		{
			currentBlockY = 0;
			currentBlockX += blockSize;
		}
		if (currentBlockX >= xSize)
		{
			sampleCount++;
			currentBlockX = 0;
			currentBlockY = 0;
			timer.stop();
			timer.start();
			finished = true;
		}
	}

	void RegisterOpenGlVertexBuffer(unsigned int bufferId)
	{
		deviceBufferPointer = GetDevicePointerFromVertexBuffer(bufferId);
	}
	
	float* deviceBufferPointer;
	int xSize = 0;
	int ySize = 0;
	int currentBlockX = 0;
	int currentBlockY = 0;
	int blockSize = 512 * 1.5;
	int sampleCount = 0;
	int blockId = 0; //Must never be the same
	bool finished = false;
	Timer timer = Timer("Frame");
};

void Render(PixelRendable& objectToRender);
