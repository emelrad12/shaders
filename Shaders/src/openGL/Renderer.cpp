#pragma once
#include "stdafx.h"
#include "Renderer.h"
#include <iostream>


void GLClearError()
{
	while (glGetError() != GLFW_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line)
{
	bool returnValue = false;
	while (true)
	{
		GLenum error = glGetError();
		if (error == GLFW_NO_ERROR)
		{
			if (returnValue == false)
			{
				return false;
			}
			return true;
		}
		returnValue = true;
		std::cout << "[OpenGL error] (" << error << "): " << function << " " << file << ":" << line << std::endl;
	}
}

void Renderer::Clear() const
{
	GLCall(glClear(GL_COLOR_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));
}

void Renderer::DrawPixels(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	// std::cout << ib.GetCount() << std::endl;
	GLCall(glDrawElements(GL_POINTS, ib.GetCount(), GL_UNSIGNED_INT, nullptr));
}
