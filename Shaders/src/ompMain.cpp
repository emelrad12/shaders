
#include "ompMain.h"
#include <chrono>
#include <vector>
#include <thread>
#include <omp.h>
#include "Timer.h"
#include <algorithm>

const int threadCount = 1;
const float heatFlowRate = 1.0f / 10;

HeatTransfer::HeatTransfer(int gridSize)
{
	gridDimension = gridSize;
	itemsPerThread = gridDimension / threadCount;
	gridStep = InitArray(gridDimension, gridDimension);
	gridStep[gridDimension / 2][gridDimension / 2] = INT32_MAX;
	// gridStep[gridDimension / 2][gridDimension / 2] = 100000;
	columnSave = InitArray(threadCount, gridDimension);
}

inline float HeatTransfer::CalculateHeatFlowAtLocation(std::vector<std::vector<float>>& grid,
                                                       std::vector<float>& columnSave, int x, int y, float rate)
{
	auto centerValue = grid[x][y];
	columnSave[y] = centerValue;
	auto change = (columnSave[y - 1] - centerValue);
	change += grid[x][y + 1] - centerValue;
	change += grid[x - 1][y] - centerValue;
	change += grid[x + 1][y] - centerValue;
	return centerValue + change * rate;
}

std::vector<std::vector<float>>& HeatTransfer::Next()
{
	Timer timer("OpenMp");
	timer.start();
	const auto itemsPerThread = gridDimension / threadCount;
#pragma omp parallel num_threads(threadCount)
	{
		const auto threadNumber = omp_get_thread_num();
		const auto loopStart = std::max(itemsPerThread * threadNumber, 1);
		const auto loopEnd = std::min(itemsPerThread * (threadNumber + 1), itemsPerThread * threadCount - 1);
		for (auto x = loopStart; x < loopEnd; x++)
		{
			for (auto y = 1; y < gridDimension - 1; y++)
			{
				auto centerValue = gridStep[x][y];
				columnSave[threadNumber][y] = centerValue;
				auto change = (columnSave[threadNumber][y - 1] - centerValue);
				change += gridStep[x][y + 1] - centerValue;
				change += gridStep[x - 1][y] - centerValue;
				change += gridStep[x + 1][y] - centerValue;
				gridStep[x][y] = centerValue + change * heatFlowRate;
			}
		}
	}
	timer.stop();
	return gridStep;
}

std::vector<std::vector<float>> HeatTransfer::InitArray(int sizeY, int sizeX)
{
	std::vector<std::vector<float>> grid;
	grid.resize(sizeY);
	for (int j = 0; j < sizeY; ++j)
	{
		grid[j].resize(sizeX);
		for (int i = 0; i < sizeX; ++i)
		{
			grid[j][i] = 0;
		}
	}
	return grid;
}
