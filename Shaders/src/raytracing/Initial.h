#pragma once
#include <vector>
#include "RaytracingGlobals.h"
#include "game/MeshLoader.h"
#include "Physics/SphereIntersection.h"
#include "Physics/World.h"

using std::unordered_map;

class RayLoader
{
public:
	World* GetWorld()
	{
		auto newWorld = new World();
		auto& world = *newWorld;

		Material mirror;
		mirror.emittance = 0;
		mirror.color = PackColor(Vector3f(1, 1, 1));
		mirror.type = MaterialType::Specular;
		mirror.glossyCoeff = 0.0;
		world.AddMaterial("mirror", mirror);

		Material foggyMirror;
		foggyMirror.emittance = 0;
		foggyMirror.color = PackColor(Vector3f(1, 1, 1));
		foggyMirror.type = MaterialType::Glossy;
		foggyMirror.glossyCoeff = 0.8;
		world.AddMaterial("foggyMirror", foggyMirror);

		Material light;
		light.emittance = 5;
		light.color = PackColor(Vector3f(1, 1, 1));
		light.type = MaterialType::Specular;
		world.AddMaterial("light", light);

		Material faintLight;
		faintLight.emittance = 1;
		faintLight.color = PackColor(Vector3f(1, 1, 1));
		faintLight.type = MaterialType::Specular;
		world.AddMaterial("faintLight", faintLight);

		Material transparent;
		transparent.emittance = 0;
		transparent.color = PackColor(Vector3f(1.0, 1.0, 1.0));
		transparent.type = MaterialType::Specular;
		transparent.transparent = true;
		transparent.transparencyCoeff = 1;
		transparent.refractionCoeff = 1.04;
		world.AddMaterial("transparent", transparent);

		Material white;
		white.emittance = 0;
		white.color = PackColor(Vector3f(1, 1, 1));
		white.type = MaterialType::Specular;
		white.glossyCoeff = 0.5;
		world.AddMaterial("white", white);

		Material green;
		green.emittance = 0;
		green.color = PackColor(Vector3f(0, 0.9, 0));
		green.type = MaterialType::Diffuse;
		green.glossyCoeff = 0.5;
		world.AddMaterial("green", green);

		Material red;
		red.emittance = 0;
		red.color = PackColor(Vector3f(0.9, 0, 0));
		red.type = MaterialType::Diffuse;
		red.glossyCoeff = 0.5;
		world.AddMaterial("red", red);

		const auto a = true;
		if (!a)
		{
			world.spheres.AddSphere(1000, Vector3f(0, 0, 0), world.GetMaterial("foggyMirror").index);
			world.spheres.AddSphere(10, Vector3f(0, 10, -25), world.GetMaterial("light").index);
			// world.spheres.AddSphere(100, Vector3f(0, 0, 400), world.GetMaterial("light").index);

		}
		if (a)
		{
			world.spheres.AddSphere(200, Vector3f(0, 200, -500), world.GetMaterial("light").index);
			world.spheres.AddSphere(100, Vector3f(150, 0, 300), world.GetMaterial("white").index);
			world.spheres.AddSphere(100, Vector3f(-150, 0, 300), world.GetMaterial("white").index);
			world.spheres.AddSphere(100, Vector3f(0, -75, 300), world.GetMaterial("white").index);
			// 
			world.spheres.AddSphere(50, Vector3f(0, 50, 150), world.GetMaterial("transparent").index);
			world.spheres.AddSphere(50, Vector3f(100, 0, 200), world.GetMaterial("red").index);
		}
		if (!a)
		{
			// world.spheres.AddSphere(25, Vector3f(-50, 0, 75), world.GetMaterial("light").index);
			// world.spheres.AddSphere(25, Vector3f(50, 0, 75), world.GetMaterial("white").index);
			// world.spheres.AddSphere(100, Vector3f(0, 0, 90), world.GetMaterial("red").index);
		}
		if (a)
		{
			int floor = world.meshes.AddMesh(Vector3f(0, 0, 0), world.GetMaterial("red").index);
			world.meshes.AddVertex(floor, -500, -50, 500);
			world.meshes.AddVertex(floor, 500, -50, 500);
			world.meshes.AddVertex(floor, 500, -50, 0);
			world.meshes.AddVertex(floor, -500, -50, 0);
			world.meshes.AddTriangle(floor, 0, 1, 2);
			world.meshes.AddTriangle(floor, 0, 2, 3);
			int wall = world.meshes.AddMesh(Vector3f(0, 0, 0), world.GetMaterial("green").index);
			world.meshes.AddVertex(wall, -500, 500, 500);
			world.meshes.AddVertex(wall, 500, 500, 500);
			world.meshes.AddVertex(wall, 500, -50, 500);
			world.meshes.AddVertex(wall, -500, -50, 500);
			world.meshes.AddTriangle(wall, 0, 1, 2);
			world.meshes.AddTriangle(wall, 0, 2, 3);
		}
		if(!a)
		{
			MeshLoader loader;
			loader.Load(world, "src/models/models.obj");
		}
		return newWorld;
	}
};
