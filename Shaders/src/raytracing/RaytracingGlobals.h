#pragma once
#include "stdafx.h"
#include <unordered_map>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#define __AllowCuda__ __host__ __device__

#ifdef _DEBUG
#define _IsDebug true
#else
#define _IsDebug false
#endif
using std::vector;
using std::string;
using std::unordered_map;
using Eigen::Vector3f;
using Eigen::Vector3;
using Eigen::Vector2f;
using Eigen::Vector2;
using Eigen::Quaternionf;
#define PI 3.141592653589793238463f
#define Deg2Rad 0.01745329f
#define UP Vector3f(0, 1, 0)

template <typename T>
void Print(T item)
{
	std::cout << item << std::endl;
}

class HasSpecificBlockId
{
public:
	int blockId = -1;
	__device__ __host__ bool IsFromCurrentBlock(int blockId) const
	{
		return this->blockId == blockId;
	}
};