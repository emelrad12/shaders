#pragma once
#include "MeshIntersection.h"
class CudaWorld;
void CalculateIntersections(RayBuffer rays, RayHitBuffer hits, CudaWorld& world, int size, int blockId);