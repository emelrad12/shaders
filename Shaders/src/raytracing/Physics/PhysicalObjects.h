#pragma once
#include "AABB.h"
#include "Material.h"
#include "../RaytracingGlobals.h"

class World;

class PhysicalObjects
{
public:
	PhysicalObjects(World& world): world(world)
	{
	}
	
	void ResizeBase(int newSize)
	{
		size = newSize;
		materialId.resize(size);
		boundingBox.resize(size);
		position.resize(size);
		rotation.resize(size);
	}

	int size = 0;
	World& world;
	vector<int> materialId;
	vector<AABB> boundingBox;
	vector<Vector3f> position;
	vector<Quaternionf> rotation;
};

class Meshes : public PhysicalObjects
{
public:
	Meshes(World& world) : PhysicalObjects(world)
	{
	}

	int AddMesh(Vector3f newPosition, int materialIndex)
	{
		auto i = IncrementSize();
		position[i] = newPosition;
		materialId[i] = materialIndex;
		vertices[i] = vector<Vector3f>();
		indices[i] = vector<Vector3<int>>();
		return i;
	}

	int IncrementSize()
	{
		size++;
		Resize(size);
		return size - 1;
	}

	void Resize(int newSize)
	{
		ResizeBase(newSize);
		vertices.resize(size);
		indices.resize(size);
	}

	void AddVertex(int meshId, Vector3f data)
	{
		vertices[meshId].push_back(data);
	}

	void AddVertex(int meshId, float v1, float v2, float v3)
	{
		vertices[meshId].push_back(Vector3f(v1, v2, v3));
		boundingBox[meshId] = AABB(meshId, *this);
	}

	void AddTriangle(int meshId, Vector3<int> data)
	{
		indices[meshId].push_back(data);
	}

	void AddTriangle(int meshId, int v1, int v2, int v3)
	{
		indices[meshId].push_back(Vector3<int>(v1, v2, v3));
	}

	vector<vector<Vector3f>> vertices;
	vector<vector<Vector3<int>>> indices;
};

class Spheres : public PhysicalObjects
{
public:
	Spheres(World& world) : PhysicalObjects(world)
	{
	}

	int IncrementSize()
	{
		size++;
		Resize(size);
		return size - 1;
	}
	
	int AddSphere(int newRadius, Vector3f newPosition, int materialIndex)
	{
		auto i = IncrementSize();
		radius[i] = newRadius;
		position[i] = newPosition;
		materialId[i] = materialIndex;
		return i;
	}

	void Resize(int newSize)
	{
		ResizeBase(newSize);
		radius.resize(size);
	}

	vector<float> radius;
};
