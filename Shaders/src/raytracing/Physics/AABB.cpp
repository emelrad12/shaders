#include "stdafx.h"
#include "AABB.h"
#include "PhysicalObjects.h"

AABB::AABB(int sphereId, Spheres& spheres)
{
	auto vec1 = Vector3f(-1, -1, -1);
	max = spheres.position[sphereId] +vec1 * spheres.radius[sphereId];
	min = spheres.position[sphereId] + vec1 * -1 * spheres.radius[sphereId];
}

AABB::AABB(int meshId, Meshes& meshes)
{
	min = Vector3f(9999999, 9999999, 9999999);
	max = Vector3f(-9999999, -9999999, -9999999);
	for (auto& ver : meshes.vertices[meshId])
	{
		min.x() = std::min(min.x(), ver.x());
		min.y() = std::min(min.y(), ver.y());
		min.z() = std::min(min.z(), ver.z());
		max.x() = std::max(max.x(), ver.x());
		max.y() = std::max(max.y(), ver.y());
		max.z() = std::max(max.z(), ver.z());
	}
}

bool AABB::IsPointInside(Vector3f& point) const
{
	return (point.x() < max.x() && point.x() > min.x()) &&
		(point.y() < max.y() && point.y() > min.y()) &&
		(point.z() < max.z() && point.z() > min.z());
}

bool AABB::DoesRayIntersect(Ray& ray) const
{
	double t1 = ray.dirInv[0] * (min[0] - ray.origin[0]);
	double t2 = ray.dirInv[0] * (max[0] - ray.origin[0]);
	double tmin = std::min(t1, t2);
	double tmax = std::max(t1, t2);

	for (int i = 1; i < 3; ++i)
	{
		t1 = ray.dirInv[i] * (min[i] - ray.origin[i]);
		t2 = ray.dirInv[i] * (max[i] - ray.origin[i]);

		tmin = std::max(tmin, std::min(t1, t2));
		tmax = std::min(tmax, std::max(t1, t2));
	}

	return tmax >= std::max(tmin, 0.0);
}
