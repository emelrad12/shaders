#include "Intersections.h"
#include "SphereIntersection.h"
#include "../Cuda/CudaWorld.h"
#include "../Cuda/CudaGlobals.h"

void CalculateIntersections(RayBuffer rays, RayHitBuffer hits, CudaWorld& world, int size, int blockId)
{
	int threads = 1024;
	int blocks = (size + threads - 1) / threads;
	SphereIntersection<<<blocks, threads>>>(rays, hits, world.spheres.positions, world.spheres.radius, world.spheres.materialIds, size, world.spheres.size, blockId);
	checkCudaError(cudaDeviceSynchronize());
	checkCudaLastError;
	MeshIntersection<<<blocks, threads>>>(rays, hits, world.meshes.transformedVertices, world.meshes.indexes, world.meshes.materialIds, world.meshes.indexesLen, world.meshes.size, size, blockId);
	checkCudaError(cudaDeviceSynchronize());
	checkCudaLastError;
}
