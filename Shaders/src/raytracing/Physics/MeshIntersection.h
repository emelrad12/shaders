#pragma once
#include "stdafx.h"
#include "PhysicalObjects.h"
#include "../Ray.h"
#include "../RaytracingGlobals.h"


__global__ void MeshIntersection(RayBuffer rays,
    RayHitBuffer hits,
    Vector3f** transformedVertexes,
    Vector3<int>** indices,
    int* materialIndixes,
    int* indexesLen,
    int meshCount,
    int maxSize,
    int blockId
);
