#include "MeshIntersection.h"

#include "raytracing/Cuda/SharedHelper.h"

__AllowCuda__ Vector3f CalculateTriangleNormal(const Vector3f& vertex0, const Vector3f& vertex1, const Vector3f& vertex2)
{
	auto U = vertex1 - vertex0;
	auto V = vertex2 - vertex0;
	Vector3f normal;
	normal.x() = (U.y() * V.z()) - (U.z() * V.y());
	normal.y() = (U.z() * V.x()) - (U.x() * V.z());
	normal.z() = (U.x() * V.y()) - (U.y() * V.x());
	return normal;
}

__AllowCuda__ bool RayIntersectsTriangle(Ray& ray, const Vector3f& vertex0, const Vector3f& vertex1, const Vector3f& vertex2, Vector3f& outIntersectionPoint)
{
	const float EPSILON = 0.0000001f;
	Vector3f edge1 = vertex1 - vertex0;
	Vector3f edge2 = vertex2 - vertex0;
	Vector3f h = ray.direction.cross(edge2);
	float a = edge1.dot(h);
	if (a > -EPSILON && a < EPSILON)
		return false; // This ray is parallel to this triangle.
	float f = 1.0 / a;
	Vector3f s = ray.origin - vertex0;
	float u = f * s.dot(h);
	if (u < 0.0 || u > 1.0)
		return false;
	Vector3f q = s.cross(edge1);
	float v = f * ray.direction.dot(q);
	if (v < 0.0 || u + v > 1.0)
		return false;
	// At this stage we can compute t to find out where the intersection point is on the line.
	float t = f * edge2.dot(q);
	if (t > EPSILON) // ray intersection
	{
		outIntersectionPoint = ray.origin + ray.direction * t;
		return true;
	}
	// This means that there is a line intersection but not a ray intersection.
	return false;
}

__global__ void MeshIntersection(RayBuffer rays,
                                 RayHitBuffer hits,
                                 Vector3f** transformedVertexes,
                                 Vector3<int>** indices,
                                 int* materialIndixes,
                                 int* indexesLen,
                                 int meshCount,
                                 int maxSize,
                                 int blockId
)
{
	const int index = getCudaIndex;
	auto currentRay = Ray();
	currentRay.origin = rays.origin[index];
	currentRay.direction = rays.direction[index];
	currentRay.blockId = rays.blockId[index];
	auto hit = RayHit();
	__shared__ Vector3f* sharedTransformedVertexes[50];
	__shared__ Vector3<int>* sharedIndices[50];
	if (threadIdx.x < meshCount)
	{
		sharedTransformedVertexes[ConflictFreePadding(threadIdx.x)] = transformedVertexes[threadIdx.x];
		sharedIndices[ConflictFreePadding(threadIdx.x)] = indices[threadIdx.x];
	}
	__syncthreads();
	float bestDistance = 999999;
	if (index >= maxSize || !currentRay.IsFromCurrentBlock(blockId))return;
	for (int meshId = 0; meshId < meshCount; meshId++)
	{
		const auto meshTransformedIndices = sharedTransformedVertexes[ConflictFreePadding(meshId)];
		const auto meshIndices = sharedIndices[ConflictFreePadding(meshId)];

		for (int indexId = 0; indexId < indexesLen[meshId]; indexId++)
		{
			const auto trigIndex = meshIndices[(indexId)];
			const auto vertex0 = meshTransformedIndices[(trigIndex.x())];
			const auto vertex1 = meshTransformedIndices[(trigIndex.y())];
			const auto vertex2 = meshTransformedIndices[(trigIndex.z())];
			Vector3f result;
			if (RayIntersectsTriangle(currentRay, vertex0, vertex1, vertex2, result))
			{
				float distance = (result - currentRay.origin).norm();
				if (distance < bestDistance && distance > 0.001)
				{
					bestDistance = distance;
					hit.materialIndex = materialIndixes[meshId];
					hit.origin = result;
					hit.normal = CalculateTriangleNormal(vertex0, vertex1, vertex2).normalized();
					hit.hasHit = true;
					hit.distance = distance;
				}
			}
		}
	}
	if (hit.hasHit)
	{
		auto oldDist = hits.distance[index];
		if (oldDist > bestDistance || !hits.IsFromCurrentBlock(index, blockId) || oldDist == 0.0f)
		{
			hits.materialIndex[index] = hit.materialIndex;
			hits.origin[index] = hit.origin;
			hits.normal[index] = hit.normal;
			hits.hasHit[index] = hit.hasHit;
			hits.distance[index] = hit.distance;
			hits.blockId[index] = blockId;
		}
	}
}
