#include "stdafx.h"
#include "SphereIntersection.h"
#include "../Cuda/CudaGlobals.h"
#include "raytracing/Cuda/SharedHelper.h"

__global__ void SphereIntersection(RayBuffer rays,
                                   RayHitBuffer hits,
                                   const Vector3f* spherePositions,
                                   const float* sphereRadiuses,
                                   const int* sphereMaterialIds,
                                   int maxSize,
                                   int sphereCount,
                                   int blockId)
{
	__shared__ Vector3f sharedSpherePositions[10];
	__shared__ float sharedSphereRadiuses[10];
	__shared__ int sharedSphereMaterialIds[10];
	const int index = getCudaIndex;
	if (threadIdx.x < sphereCount)
	{
		sharedSpherePositions[ConflictFreePadding(threadIdx.x)] = spherePositions[threadIdx.x];
		sharedSphereRadiuses[ConflictFreePadding(threadIdx.x)] = sphereRadiuses[threadIdx.x];
		sharedSphereMaterialIds[ConflictFreePadding(threadIdx.x)] = sphereMaterialIds[threadIdx.x];
	}
	__syncthreads();
	auto currentRay = Ray();
	currentRay.origin = rays.origin[index];
	currentRay.direction = rays.direction[index];
	currentRay.blockId = rays.blockId[index];
	if (index >= maxSize || !currentRay.IsFromCurrentBlock(blockId))return;
	float minDist = 999999;
	RayHit localHit = RayHit();
	localHit.hasHit = false;
	Vector3f bestSpherePosition;
	float bestFirstForwardPoint = 0;
	for (int i = 0; i < sphereCount; i++)
	{
		Vector3f relativePosition = currentRay.origin - sharedSpherePositions[ConflictFreePadding(i)];
		const float sphereRadius = sharedSphereRadiuses[ConflictFreePadding(i)];
		const float a = 1.0f;
		const float b = currentRay.direction.dot(relativePosition) * 2.0f;
		const float c = relativePosition.dot(relativePosition) - sphereRadius * sphereRadius;
		const float d = b * b - 4.0f * a * c;
		if (d < 0.0f)
		{
			continue;
		}

		const float sqrtd = sqrt(d);
		const float point1 = (-b - sqrtd) / (2 * a);
		const float point2 = (-b + sqrtd) / (2 * a);
		bool inside = false;
		if (point1 < 0 && point2 > 0)
		{
			inside = true;
		}

		const float firstForwardPoint = inside ? point2 : point1;
		if (firstForwardPoint > 0.001f && firstForwardPoint < minDist)
		{
			bestSpherePosition = sharedSpherePositions[ConflictFreePadding(i)];
			localHit.materialIndex = sharedSphereMaterialIds[ConflictFreePadding(i)];
			localHit.hasHit = true;
			localHit.wasInside = inside;
			minDist = firstForwardPoint;
			bestFirstForwardPoint = firstForwardPoint;
		}
	}
	if (localHit.hasHit)
	{
		localHit.origin = bestFirstForwardPoint * currentRay.direction + currentRay.origin;
		localHit.normal = (localHit.origin - bestSpherePosition).normalized();
		localHit.distance = bestFirstForwardPoint;
		localHit.blockId = blockId;
		if (localHit.wasInside)
		{
			localHit.normal *= -1;
		}
		hits.materialIndex[index] = localHit.materialIndex;
		hits.hasHit[index] = localHit.hasHit;
		hits.distance[index] = localHit.distance;
		hits.origin[index] = localHit.origin;
		hits.normal[index] = localHit.normal;
		hits.blockId[index] = localHit.blockId;
		hits.wasInside[index] = localHit.wasInside;
	}
}
