#pragma once
#include "../RaytracingGlobals.h"
#include "../Ray.h"

class Spheres;
class Meshes;

class AABB
{
public:
	AABB()
	{
	}

	AABB(Vector3f& min, Vector3f& max): min(min), max(max)
	{
	}

	AABB(int sphereId, Spheres& spheres);
	AABB(int meshId, Meshes& meshes);
	__AllowCuda__ bool IsPointInside(Vector3f& point) const;
	__AllowCuda__ bool DoesRayIntersect(Ray& ray) const;

	Vector3f min;
	Vector3f max;
};
