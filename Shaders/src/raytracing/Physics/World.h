#pragma once
#include "../RaytracingGlobals.h"
#include "PhysicalObjects.h"

class World
{
public:
	World() : spheres(Spheres( *this)), meshes(Meshes(*this))
	{
	}

	void AddMaterial(string name, Material value)
	{
		if (materialMapping.find(name) != materialMapping.end())
		{
			throw "Item with name " + name + " already exists";
		}
		value.index = materials.size();
		materials.push_back(value);
		materialMapping.emplace(name, materials.size() - 1);
	}

	Material& GetMaterial(string name)
	{
		if (materialMapping.find(name) == materialMapping.end())
		{
			throw "Item with name " + name + " doesn't exists";
		}
		return materials[materialMapping[name]];
	}

	Spheres spheres;
	Meshes meshes;
	unordered_map<string, uint32_t> materialMapping = unordered_map<string, uint32_t>();
	vector<Material> materials = vector<Material>();
};
