#include "Material.h"

#include "raytracing/Cuda/CudaGlobals.h"

MaterialBuffer::MaterialBuffer(int size)
{
	AllocUnManagedArray(color, size);
	AllocUnManagedArray(type, size);
	AllocUnManagedArray(emittance, size);
	AllocUnManagedArray(refractionCoeff, size);
	AllocUnManagedArray(transparencyCoeff, size);
	AllocUnManagedArray(glossyCoeff, size);
	AllocUnManagedArray(transparent, size);
	AllocUnManagedArray(index, size);
}

void MaterialBuffer::Add(Material material)
{
	
	auto id = material.index;
	CopyVariableToUnmanagedArray(color, id, material.color);
	CopyVariableToUnmanagedArray(type, id, material.type);
	CopyVariableToUnmanagedArray(emittance, id, material.emittance);
	CopyVariableToUnmanagedArray(refractionCoeff, id, material.refractionCoeff);
	CopyVariableToUnmanagedArray(transparencyCoeff, id, material.transparencyCoeff);
	CopyVariableToUnmanagedArray(glossyCoeff, id, material.glossyCoeff);
	CopyVariableToUnmanagedArray(transparent, id, material.transparent);
}
