#pragma once
#include "PhysicalObjects.h"
#include "../Ray.h"
#include "../RaytracingGlobals.h"
__global__ void SphereIntersection(RayBuffer rays,
									RayHitBuffer hits,
                                   const Vector3f* spherePositions,
                                   const float* sphereRadiuses,
                                   const int* sphereMaterialIds,
                                   int maxSize,
                                   int sphereCount,
                                   int blockId);
