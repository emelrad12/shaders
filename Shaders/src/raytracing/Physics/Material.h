#pragma once
#include <Eigen/Dense>
#include "../RaytracingGlobals.h"
#define colorStorage float
enum class MaterialType
{
	Diffuse,
	Glossy,
	Specular
};

__AllowCuda__ inline Vector3f UnpackColor(Vector3<colorStorage> data)
{
	return data.cast<float>() / 255.0f;
}

__AllowCuda__ inline Vector3<colorStorage> PackColor(Vector3f data)
{
	return (data * 255).cast<colorStorage>();
}

class Material
{
public:
	Vector3<colorStorage> color;
	MaterialType type;
	float emittance = 0;
	float refractionCoeff = 1;
	float transparencyCoeff = 0;
	float glossyCoeff = 0;
	short index = -1;
	bool transparent = false;
};

class MaterialBuffer
{
public:
	MaterialBuffer() {};
	MaterialBuffer(int size);
	void Add(Material material);
	Vector3<colorStorage>* color;
	MaterialType* type;
	float* emittance;
	float* refractionCoeff;
	float* transparencyCoeff;
	float* glossyCoeff;
	short* index;
	bool* transparent;
};
