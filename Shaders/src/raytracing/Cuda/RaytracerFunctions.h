#pragma once

#include "../RaytracingGlobals.h"
#include "cuda_runtime.h"
class MaterialBuffer;
class RayBuffer;
class RayHitBuffer;
class Material;
class Ray;
class RayHit;
__AllowCuda__ extern Quaternionf GetQuaternionFromTwoVectors(Vector3f a, Vector3f b);
__AllowCuda__ extern Vector3f RandomVectorInHemisphere(Vector3f& normal, uint32_t seed);
__AllowCuda__ extern void AddSampleToSamples(Vector3f& sample, Vector3f& previous, int totalSamples, bool flush);
__global__ extern void SumUpResults(RayHitBuffer* hits, RayBuffer* rays, MaterialBuffer materials, int size, int generations, int blockId);
__global__ extern void CastCameraRays(RayBuffer rays, Vector3f CameraPosition, Quaternionf cameraRotation, int blockSize, int xSize, int ySize, int currentX, int currentY, int blockId);
__global__ extern void AddSamplesAndWriteToBuffer(RayHitBuffer hits, float* bufferPointer, int totalSamples, int startY, int startX, int endY, int endX, int xSize, int ySize, int blockSize, bool flush);
__global__ extern void TestKernel(Ray* hits, int length, int block);
__global__ extern void CalculateAllReflections(RayBuffer newGeneration, RayBuffer oldGeneration, MaterialBuffer materials, RayHitBuffer hits, int blockId, int maxSize);
