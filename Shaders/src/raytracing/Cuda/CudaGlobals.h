#pragma once
#include "../RaytracingGlobals.h"
#define getCudaIndex (threadIdx.x + blockDim.x * blockIdx.x)
#define printCudaError \
	{if (cudaSuccess != error)\
	{\
		std::cout << "Error!" << std::endl;\
		std::cout << cudaGetErrorString(error) << std::endl;\
		__debugbreak();\
	}}

#define checkCudaLastError\
	{const auto error = cudaGetLastError();\
		printCudaError}

#define checkCudaError(item)\
	{const auto error = item;\
		printCudaError}

template <typename T>
void AllocManagedArray(T*& item, int size)
{
	checkCudaError(cudaMallocManaged(&item, size * sizeof(*item)));
}

template <typename T>
void AllocUnManagedArray(T*& item, int size)
{
	checkCudaError(cudaMalloc(&item, size * sizeof(*item)));
}

template <typename T>
void DeallocUnManagedArray(T*& item)
{
	checkCudaError(cudaFree(&item));
}

template <typename T>
void CopyToUnmanagedArray(T* to, T* from, int size)
{
	checkCudaError(cudaMemcpy(to, from, size * sizeof(T), cudaMemcpyDefault));
}

template <typename T>
void CopyToManagedVariable(T& item, T value)
{
	checkCudaError(cudaMemcpyToSymbol(item, &value, sizeof(item)), 0, cudaMemcpyDefault);
	cudaDeviceSynchronize();
	item = value;
}

template <typename T>
void CopyVariableToUnmanagedArray(T* target, int index, T value)
{
	checkCudaError(cudaMemcpy(target + index, &value, sizeof(T), cudaMemcpyDefault));
}

template <typename T>
void Resize(T* item, int size, int oldSize)
{
	auto oldItem = item;
	AllocManagedArray(item, size);
	cudaMemcpy(oldItem, item, oldSize, cudaMemcpyDeviceToDevice);
	cudaFree(oldItem);
	checkCudaLastError;
}

__global__ inline void PrintGpuArray(int* input, int size, int stride = 1)
{
	printf("new\n");
	for (int i = 0; i < size; i += stride)
	{
		printf("data: %d::%d\n", i, input[i]);
	}
}