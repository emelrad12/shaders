#include "FastRandoms.h"
std::mt19937 mersenneTwister;
std::uniform_real_distribution<double> uniform;
__device__ Vector3f* randomNormals;
Vector3f* randomNormalsHost;
__device__ int randomNormalsSize;

#define RND (2.0*uniform(mersenneTwister)-1.0)
#define RND2 (uniform(mersenneTwister))

void InitRandoms()
{
	CopyToManagedVariable(randomNormalsSize, 1000);
	AllocUnManagedArray(randomNormals, randomNormalsSize);
	randomNormalsHost = new Vector3f[randomNormalsSize];
	CopyToManagedVariable(randomNormals, randomNormals);
	RecalculateRandomNormals();
}

void RecalculateRandomNormals()
{
	for (size_t i = 0; i < randomNormalsSize; i++)
	{
		float u1 = RND2;
		float u2 = RND2;
		const float r = sqrt(1.0 - u1 * u1);
		const float phi = 2 * PI * u2;
		auto randomNormal = Vector3f(cos(phi) * r, 1, sin(phi) * r).normalized();
		randomNormalsHost[i] = randomNormal;
	}
	CopyToUnmanagedArray(randomNormals, randomNormalsHost, randomNormalsSize);
}
