#pragma once
#include "stdafx.h"
#include "CudaWorld.h"
#include "../Ray.h"
#include "../../openGL/PixelRenderer.h"
#include "game/CharacterController.h"

void TestCudaRaytracer();

class World;

class CudaRaytracer : public PixelRendable
{
public:
	CudaRaytracer(int maxDepth, World& world, int xSize, int ySize);
	void Update() override;
	void TraceGeneration(int generation);
	CudaWorld world;
	CharacterController controller;
	int maxDepth;
	RayBuffer* rays;
	RayHitBuffer* raysHits;
	RayBuffer* localRays;
	RayHitBuffer* localRaysHits;
	
	cudaStream_t streams[3];
	vector<Vector3f> pixels;
};

class CudaRaytracerWorker
{
	CudaRaytracer& parent;
};
