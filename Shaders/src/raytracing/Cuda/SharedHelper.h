#pragma once
#include <crt/host_defines.h>
void __syncthreads(); //Definition provided by cuda somewhere somehow. But apparently not in the headers

__device__ constexpr int ConflictFreePadding(int input)
{
	const int logBanks = 5;
	const auto padding = input >> logBanks;
	return padding + input;
}

template <typename T>
class Array3
{
public:

	__AllowCuda__ T& operator[](int index)
	{
		switch (index)
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		default: ;
		}
	}

	T x;
	T y;
	T z;
};
