#include "stdafx.h"
#include "CudaWorld.h"
#include "CudaGlobals.h"

__global__ void TransformVerticies(Vector3f* position,
                                   Quaternionf* rotation,
                                   Vector3f** verticies,
                                   Vector3f** transformedVerticies,
                                   int* vertexCount,
                                   int size)
{
	int meshIndex = getCudaIndex;
	if (meshIndex < size)
	{
		for (size_t vertexIndex = 0; vertexIndex < vertexCount[meshIndex]; vertexIndex++)
		{
			transformedVerticies[meshIndex][vertexIndex] = rotation[meshIndex] * verticies[meshIndex][vertexIndex] + position[meshIndex];
		}
	}
}

void CudaPhysicalObjects::InitializeBase(PhysicalObjects& physicalObjects)
{
	AllocUnManagedArray(positions, size);
	AllocUnManagedArray(rotations, size);
	AllocUnManagedArray(materialIds, size);
	if (size > 0)
	{
		CopyToUnmanagedArray(positions, &physicalObjects.position[0], size);
		CopyToUnmanagedArray(rotations, &physicalObjects.rotation[0], size);
		CopyToUnmanagedArray(materialIds, &physicalObjects.materialId[0], size);
	}
}

void CudaPhysicalObjects::Synchronize(PhysicalObjects& physicalObjects)
{
	DeallocUnManagedArray(positions);
	DeallocUnManagedArray(rotations);
	DeallocUnManagedArray(materialIds);
	InitializeBase(physicalObjects);
}

void CudaSpheres::Synchronize()
{
	CudaPhysicalObjects::Synchronize(world.spheres);
	DeallocUnManagedArray(radius);
	Initialize();
}

void CudaSpheres::Initialize()
{
	size = world.spheres.size;
	InitializeBase(world.spheres);

	AllocUnManagedArray(radius, size);
	if (size > 0)
	{
		CopyToUnmanagedArray(radius, &world.spheres.radius[0], size);
	}
}

void CudaMeshes::Synchronize()
{
}

void CudaMeshes::Initialize()
{
	size = world.meshes.size;
	InitializeBase(world.meshes);
	if (size <= 0) return;
	const auto tempVerticies = new Vector3f*[size];
	const auto tempIndexes = new Vector3<int>*[size];
	const auto tempTransformedVertices = new Vector3f*[size];
	const auto tempIndexesLen = new int[size];

	AllocUnManagedArray(rotations, world.meshes.rotation.size());
	AllocUnManagedArray(verticies, size);
	AllocUnManagedArray(indexes, size);
	AllocUnManagedArray(transformedVertices, size);
	AllocUnManagedArray(indexesLen, size);
	if(size > 0)
	{
		CopyToUnmanagedArray(rotations, &world.meshes.rotation[0], world.meshes.rotation.size());
		for (int i = 0; i < size; i++)
		{
			AllocUnManagedArray(tempVerticies[i], world.meshes.vertices[i].size());
			CopyToUnmanagedArray(tempVerticies[i], &world.meshes.vertices[i][0], world.meshes.vertices[i].size());

			AllocUnManagedArray(tempIndexes[i], world.meshes.indices[i].size());
			CopyToUnmanagedArray(tempIndexes[i], &world.meshes.indices[i][0], world.meshes.indices[i].size());

			AllocUnManagedArray(tempTransformedVertices[i], world.meshes.vertices[i].size()); //Not applicable on cpu
			CopyToUnmanagedArray(tempTransformedVertices[i], &world.meshes.vertices[i][0], world.meshes.vertices[i].size());

			tempIndexesLen[i] = world.meshes.indices[i].size();
		}



		CopyToUnmanagedArray(verticies, tempVerticies, size);
		CopyToUnmanagedArray(indexes, tempIndexes, size);
		CopyToUnmanagedArray(transformedVertices, tempTransformedVertices, size);
		CopyToUnmanagedArray(indexesLen, tempIndexesLen, size);
	}

	delete[] tempVerticies;
	delete[] tempIndexes;
	delete[] tempTransformedVertices;
}

void CudaMeshes::StartTransformVerticies()
{
}

void CudaWorld::Synchronize()
{
	spheres.Synchronize();
	meshes.Synchronize();
}

void CudaWorld::Initialize(World& world)
{
	auto size = world.materials.size();
	materials = MaterialBuffer(world.materials.size());
	for (auto i = 0; i < size; i++)
	{
		materials.Add(world.materials[i]);
	}
}
