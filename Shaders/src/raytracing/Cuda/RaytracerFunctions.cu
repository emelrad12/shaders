#include "stdafx.h"
#include "RaytracerFunctions.h"
#include <utility>
#include "cassert"
#include "CudaGlobals.h"
#include "FastRandoms.h"
#include "SharedHelper.h"
#include "../Ray.h"
#include "Timer.h"

inline __device__ __host__ Vector3f Reflect(const Vector3f& normal, const Vector3f& incident)
{
	return (incident - 2 * incident.dot(normal) * normal).normalized();
}


inline __device__ __host__ Vector3f Refract(const Vector3f& normal, const Vector3f& incident, float n1, float n2)
{
	{
		float n = n1 / n2;
		float cosI = -normal.dot(incident);
		float sinT2 = n * n * (1 - cosI * cosI);
		if (sinT2 > 1)
		{
			return Vector3f(1000, 0, 0);
		}
		float cosT = sqrtf(1 - sinT2);
		return n * incident + normal * (n * cosI - cosT);
	}
}

__device__ __host__ Quaternionf GetQuaternionFromTwoVectors(Vector3f a, Vector3f b)
{
	auto dot = a.dot(b);
	auto xUnitVec = Vector3f(1, 0, 0);
	auto yUnitVec = Vector3f(0, 1, 0);
	if (dot < -0.999999)
	{
		auto temp = xUnitVec.cross(a);
		if (temp.norm() < 0.000001)
		{
			temp = yUnitVec.cross(a);
		}
		temp.normalize();
		return Quaternionf(Eigen::AngleAxis<float>(PI, temp));
	}
	if (dot > 0.999999)
	{
		return Quaternionf::Identity();
	}

	auto cross = a.cross(b);
	auto result = Quaternionf(1 + dot, cross.x(), cross.y(), cross.z()).normalized();
	return result;
}

__device__ __host__ void AddSampleToSamples(Vector3f& sample, Vector3f& previous, int totalSamples, bool flush)
{
	if (flush)
	{
		previous = sample;
	}
	else
	{
		previous *= totalSamples;
		previous += sample;
		previous /= totalSamples + 1;
	}
}

__device__ __host__ Vector3f RandomVectorInHemisphere(Vector3f& normal, uint32_t seed)
{
	auto randomNormalsIndex = (seed + clock()) % randomNormalsSize;
	const auto& randomVector = randomNormals[randomNormalsIndex];
	const auto rotation = GetQuaternionFromTwoVectors(UP, normal);
	auto result = rotation * randomVector;
	assert(result.dot(normal) >= 0);
	return result;
}

__global__ void SumUpResults(RayHitBuffer* hits, RayBuffer* rays, MaterialBuffer materials, int size, int generations, int blockId)
{
	const auto index = getCudaIndex;
	if (index < size)
	{
		for (auto generation = generations - 1; generation >= 0; generation--)
		{
			auto& currentGeneration = hits[generation];

			auto currentHit = RayHit();
			currentHit.color = currentGeneration.color[index];
			currentHit.blockId = currentGeneration.blockId[index];
			currentHit.hasHit = currentGeneration.hasHit[index];
			currentHit.materialIndex = currentGeneration.materialIndex[index];
			currentHit.normal = currentGeneration.normal[index];

			if (currentHit.hasHit && currentHit.IsFromCurrentBlock(blockId))
			{
				auto currentHitMaterial = Material();
				currentHitMaterial.color = materials.color[currentHit.materialIndex];
				currentHitMaterial.emittance = materials.emittance[currentHit.materialIndex];
				currentHit.color = PackColor(currentHitMaterial.emittance * UnpackColor(currentHitMaterial.color));
				if (generation != generations - 1)
				{
					RayHit nextHit = RayHit();
					nextHit.color = hits[generation + 1].color[index];
					nextHit.hasHit = hits[generation + 1].hasHit[index];
					nextHit.blockId = hits[generation + 1].blockId[index];

					if (nextHit.hasHit && nextHit.IsFromCurrentBlock(blockId))
					{
						float cosTheta = rays[generation + 1].direction[index].dot(currentHit.normal);
						Vector3f brdf = UnpackColor(currentHitMaterial.color) * cosTheta;
						currentHit.color += PackColor(brdf.cwiseProduct(UnpackColor(nextHit.color)));
					}
				}
			}
			else
			{
				currentHit.color = PackColor(Vector3f(0, 0, 0));
			}

			currentGeneration.color[index] = currentHit.color;
		}
	}
}

__global__ void CalculateAllReflections(RayBuffer newGeneration, RayBuffer oldGeneration, MaterialBuffer materials, RayHitBuffer hits, int blockId, int maxSize)
{
	auto index = getCudaIndex;
	if (index >= maxSize) return;
	auto itemIndex = index;
	if (hits.hasHit[itemIndex] && hits.IsFromCurrentBlock(itemIndex, blockId))
	{
		auto materialType = materials.type[hits.materialIndex[index]];
		if (materialType == MaterialType::Diffuse)
		{
			auto direction = RandomVectorInHemisphere(hits.normal[itemIndex], index + blockId);
			auto ray = Ray(hits.origin[itemIndex], direction, blockId);
			newGeneration.Set(ray, itemIndex);
		}

		if (materialType == MaterialType::Specular)
		{
			auto& incident = oldGeneration.direction[itemIndex];
			auto& normal = hits.normal[itemIndex];
			auto direction = Reflect(normal, incident);
			auto ray = Ray(hits.origin[itemIndex], direction, blockId);
			newGeneration.Set(ray, itemIndex);
		}

		if (materialType == MaterialType::Glossy)
		{
			auto& incident = oldGeneration.direction[itemIndex];
			auto& normal = hits.normal[itemIndex];
			auto glossyCoeff = materials.glossyCoeff[hits.materialIndex[itemIndex]];
			auto direction = Reflect(normal, incident);
			direction *= 1 - glossyCoeff;
			direction += RandomVectorInHemisphere(hits.normal[itemIndex], index + blockId) * glossyCoeff;
			direction.normalize();
			auto ray = Ray(hits.origin[itemIndex], direction, blockId);
			newGeneration.Set(ray, itemIndex);
		}
	}
}

__global__ void CastCameraRays(RayBuffer rays, Vector3f CameraPosition, Quaternionf cameraRotation, int blockSize, int xSize, int ySize, int currentX, int currentY, int blockId)
{
	int index = getCudaIndex;
	int remainingSizeX = fminf(blockSize, xSize - currentX);
	int remainingSizeY = fminf(blockSize, ySize - currentY);
	if (index >= remainingSizeX * remainingSizeY)return;
	int x = currentX + index % remainingSizeX;
	int y = currentY + index / remainingSizeX;
	float w = xSize;
	float h = ySize;
	float fovx = PI / 4;
	float fovy = h / w * fovx;
	auto ray = Ray(std::move(CameraPosition), cameraRotation * Vector3f(
		               ((2 * x - w) / w) * tan(fovx),
		               ((2 * y - h) / h) * tan(fovy),
		               1).normalized(), blockId);
	rays.Set(ray, index);
}

__global__ void AddSamplesAndWriteToBuffer(RayHitBuffer hits, float* bufferPointer, int totalSamples, int startY, int startX, int endY, int endX, int xSize, int ySize, int blockSize, bool flush)
{
	auto index = getCudaIndex;
	auto remainingXSize = __min(blockSize, (xSize - startX));
	auto y = startY + index / remainingXSize;
	auto x = startX + index % remainingXSize;
	if (y >= endY || x >= endX)
	{
		return;
	}
	auto realIndex = y * xSize + x;
	auto indexInBuffer = realIndex * 5;
	auto currentColor = UnpackColor(hits.color[index]).normalized();
	auto previous = Vector3f(bufferPointer[indexInBuffer + 2], bufferPointer[indexInBuffer + 3], bufferPointer[indexInBuffer + 4]);
	AddSampleToSamples(currentColor, previous, totalSamples, flush);
	bufferPointer[indexInBuffer] = (static_cast<float>(x) / xSize) * 2.0f - 1.0f;
	bufferPointer[indexInBuffer + 1] = (static_cast<float>(y) / ySize) * 2.0f - 1.0f;

	bufferPointer[indexInBuffer + 2] = previous.x();
	bufferPointer[indexInBuffer + 3] = previous.y();
	bufferPointer[indexInBuffer + 4] = previous.z();

	// if (y == startY || x == startX)
	// {
	// 	bufferPointer[indexInBuffer + 2] = 1;
	// 	bufferPointer[indexInBuffer + 3] = 1;
	// 	bufferPointer[indexInBuffer + 4] = 1;
	// }
}

__global__ void TestKernel(Ray* hits, int length, int block)
{
	int total = 0;
	for (size_t i = 0; i < length; i++)
	{
		if (hits[i].IsFromCurrentBlock(block))
		{
			total++;
		}
	}
	if (total == 0)
	{
		for (size_t i = 0; i < length; i++)
		{
			if (!hits[i].IsFromCurrentBlock(block))
				printf("%d   ///   %d \n", block, hits[i].blockId);
		}
	}
	printf("%d \n", total);
}
