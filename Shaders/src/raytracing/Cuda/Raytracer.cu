#include "Raytracer.h"
#include "../Initial.h"

#include "CudaGlobals.h"
#include "cuda_runtime.h"
#include "FastRandoms.h"
#include "../../Timer.h"
#include "../Physics/Intersections.h"
#include "RaytracerFunctions.h"
#include <thrust/execution_policy.h>

void CudaRaytracer::TraceGeneration(int generation)
{
	Timer timer = Timer("Generation: " + std::to_string(generation));
	timer.start();
	int totalSize = blockSize * blockSize;
	int threads = 256;
	int blocks = (totalSize + threads - 1) / threads;
	if (generation == 0)
	{
		CastCameraRays << <blocks, threads >> >(localRays[generation], controller.position, controller.rotation, blockSize, xSize, ySize, currentBlockX, currentBlockY, blockId);
	}
	CalculateIntersections(localRays[generation], localRaysHits[generation], world, totalSize, blockId);
	CalculateAllReflections << <blocks, threads >> >(localRays[generation + 1], localRays[generation], world.materials, localRaysHits[generation], blockId, totalSize);

	// timer.stop();
}

CudaRaytracer::CudaRaytracer(int maxDepth, World& world, int xSize, int ySize) : world(CudaWorld(world)), maxDepth(maxDepth)
{
	this->xSize = xSize;
	this->ySize = ySize;

	pixels = vector<Vector3f>(xSize * ySize);
	for (size_t i = 0; i < xSize * ySize; i++)
	{
		pixels[i] = Vector3f(0, 0, 0);
	}
	int totalSize = blockSize * blockSize;
	localRaysHits = new RayHitBuffer[totalSize];
	localRays = new RayBuffer[totalSize];

	for (int i = 0; i < 3; i++)
	{
		cudaStreamCreate(&streams[i]);
	}

	AllocUnManagedArray(rays, maxDepth);
	AllocUnManagedArray(raysHits, maxDepth);

	for (int i = 0; i < maxDepth; i++)
	{
		localRaysHits[i] = RayHitBuffer(totalSize);
		localRays[i] = RayBuffer(totalSize);
	}
	CopyToUnmanagedArray(raysHits, localRaysHits, maxDepth);
	CopyToUnmanagedArray(rays, localRays, maxDepth);
}

void CudaRaytracer::Update()
{
	controller.Update();
	Timer timer = Timer("Block");
	timer.start();
	RecalculateRandomNormals();
	int totalSize = blockSize * blockSize;
	int threads = 512;
	int blocks = (totalSize + threads - 1) / threads;
	for (int i = 0; i < maxDepth - 1; i++)
	{
		TraceGeneration(i);
	}
	SumUpResults << <blocks, threads >> >(raysHits, rays, world.materials, totalSize, maxDepth, blockId);
	checkCudaLastError;
	checkCudaError(cudaDeviceSynchronize());
	int maxX = std::min(currentBlockX + blockSize, xSize);
	int maxY = std::min(currentBlockY + blockSize, ySize);
	if(controller.changed)
	{
		sampleCount = 0;
	}
	AddSamplesAndWriteToBuffer << <blocks, threads >> >(localRaysHits[0], deviceBufferPointer, sampleCount, currentBlockY, currentBlockX, maxY, maxX, xSize, ySize, blockSize, controller.changed);
	checkCudaLastError;
	checkCudaError(cudaDeviceSynchronize());
	// timer.stop();
}

void TestCudaRaytracer()
{
	InitRandoms();
	// auto tracer = CudaRaytracer(5, *RayLoader().GetWorld(), 3840, 2160);
	auto tracer = CudaRaytracer(5, *RayLoader().GetWorld(), 3840/2, 2160/2);
	Render(tracer);
}
