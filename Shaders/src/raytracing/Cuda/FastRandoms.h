#pragma once
#include "stdafx.h"
#include "CudaGlobals.h"
#include "../RaytracingGlobals.h"
__device__ extern Vector3f* randomNormals;
__device__ extern int randomNormalsSize;
void InitRandoms();
void RecalculateRandomNormals();