#pragma once
#include "../RaytracingGlobals.h"
#include "../Physics/Material.h"
#include "../Physics/World.h"

class CudaPhysicalObjects
{
public:
	CudaPhysicalObjects(World& world) : world(world)
	{
	}

	void InitializeBase(PhysicalObjects& physicalObjects);

	Material& GetMaterial(int index) const
	{
		return world.materials[index];
	}

	void Synchronize(PhysicalObjects& physicalObjects);

	Vector3f* positions;
	Quaternionf* rotations;
	int* materialIds;
	AABB* boundingBox;
	World& world;
	int size;
};

class CudaSpheres : public CudaPhysicalObjects
{
public:
	CudaSpheres(World& world): CudaPhysicalObjects(world)
	{
		Initialize();
	}
	
	void Synchronize();

	void Initialize();
	float* radius;
};

class CudaMeshes : public CudaPhysicalObjects
{
public:
	CudaMeshes(World& world) : CudaPhysicalObjects(world)
	{
		Initialize();
	}

	void Synchronize();

	void Initialize();
	void StartTransformVerticies();
	Vector3f** verticies;
	Vector3<int>** indexes;
	int* indexesLen;
	Vector3f** transformedVertices;
	Quaternionf* rotations;
};

class CudaWorld
{
public:
	CudaWorld(World& world) : meshes(world), spheres(world)
	{
		Initialize(world);
	}

	void Synchronize();

	void Initialize(World& world);
	CudaMeshes meshes;
	CudaSpheres spheres;
	MaterialBuffer materials;
	
	MaterialBuffer materialsDevice;
};
