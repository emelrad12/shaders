#include "Ray.h"

RayHitBuffer::RayHitBuffer(int len)
{
	this->len = len;
	AllocUnManagedArray(materialIndex, len);
	AllocUnManagedArray(normal, len);
	AllocUnManagedArray(origin, len);
	AllocUnManagedArray(color, len);
	AllocUnManagedArray(distance, len);
	AllocUnManagedArray(hasHit, len);
	AllocUnManagedArray(wasInside, len);
	AllocUnManagedArray(outgoingRayIndex, len);
	AllocUnManagedArray(blockId, len);
	AllocUnManagedArray(materialIndex, len);
}

RayBuffer::RayBuffer(int len)
{
	this->len = len;
	AllocUnManagedArray(origin, len);
	AllocUnManagedArray(direction, len);
	AllocUnManagedArray(dirInv, len);
	AllocUnManagedArray(hitMaterial, len);
	AllocUnManagedArray(taken, len);
	AllocUnManagedArray(blockId, len);
}

void RayBuffer::Set(Ray ray, int id)
{
	origin[id] = ray.origin;
	direction[id] = ray.direction;
	dirInv[id] = ray.dirInv;
	blockId[id] = ray.blockId;
	taken[id] = ray.taken;
	hitMaterial[id] = ray.hitMaterial;
}

