#pragma once
#include <Eigen/Dense>
#include "RaytracingGlobals.h"
#include "Cuda/CudaGlobals.h"
#include "Physics/Material.h"

class RayHit : public HasSpecificBlockId
{
public:
	int materialIndex;
	Vector3f normal;
	Vector3f origin;
	Vector3<colorStorage> color;
	float distance;
	bool hasHit = false;
	bool wasInside = false;
	int outgoingRayIndex;
};

class RayHitBuffer
{
public:
	RayHitBuffer()
	{
	};
	RayHitBuffer(int len);
	int* materialIndex;
	Vector3f* normal;
	Vector3f* origin;
	Vector3<colorStorage>* color;
	float* distance;
	bool* hasHit;
	bool* wasInside;
	int* outgoingRayIndex;
	int* blockId;
	int len;
	__device__ bool IsFromCurrentBlock(int index, int blockId) const
	{
		return this->blockId[index] == blockId;
	}
};

class Ray : public HasSpecificBlockId
{
public:
	__device__ Ray()
	{
	}

	__device__ Ray(Vector3f origin, Vector3f direction, int blockId): origin(origin), direction(direction)
	{
		this->blockId = blockId;
		dirInv = Vector3f(1 / direction.x(), 1 / direction.y(), 1 / direction.z());
	}

	Vector3f origin;
	Vector3f direction;
	Vector3f dirInv;
	bool hitMaterial = false;
	bool taken = false;
};

class RayBuffer
{
public:
	RayBuffer()
	{
	}
	RayBuffer(int len);
	Vector3f* origin;
	Vector3f* direction;
	Vector3f* dirInv;
	bool* hitMaterial;
	bool* taken;
	int* blockId;
	int len;
	__device__ void Set(Ray ray, int id);
	
	__device__ bool IsFromCurrentBlock(int index, int blockId) const
	{
		return this->blockId[index] == blockId;
	}
};
