#version 460 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

out vec4 o_Color;

void main()
{
	gl_Position = position;
	o_Color = color;
};