#version 460 core
in vec4 o_Color;
layout(location = 0) out vec4 color;

void main()
{
	color = o_Color;
}