#include "cuda_runtime.h"
#include <optix_device.h>
#include "raytracing/Cuda/CudaShaderCommon.h"

extern "C" {
    __constant__ Params params;
}

struct RayGenData
{
    float r, g, b;
};

extern "C"
__global__ void __raygen__draw_solid_color()
{
    uint3 launch_index = optixGetLaunchIndex();
    RayGenData* rtData = (RayGenData*)optixGetSbtDataPointer();
    params.image[launch_index.y * params.image_width + launch_index.x] = make_uchar4(255 * 1, 255 * 0, 255 * 0, 1);
}