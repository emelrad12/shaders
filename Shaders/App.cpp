#include "src/openGL/openGLMain.h"
#include "src/openGL/OmpRenderer.h"
#include "src/OpenMpStress.h"
#include "src/raytracing/Cuda/Raytracer.h"
extern void CudaMain();

int main()
{
	// StartTests();
	// runStress();
	// runOpenGL();
	//drawHeat();
	// CudaMain();
	TestCudaRaytracer();
	return 0;
}
